export const theme = {
  primary: "#1A1E29",
  secondary: "#232937",
  focus: "#4974FF",
  green: "#139A78",
  red: "#9A1344",
  white: "#FFFFFF",
};
