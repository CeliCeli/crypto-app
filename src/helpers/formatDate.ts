export const formatDate = (initDate: string): string => {
  const date = new Date(initDate);
  const day: string = String(date.getDate());
  const month: string = String(date.getMonth() + 1);
  const hh: string = String(date.getHours());
  const mm: string = String(date.getMinutes());
  return `${hh}:${mm}`;
};
