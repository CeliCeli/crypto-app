export const camelize = (str: string): string => {
  const words = str.split("");

  const capitalized = words
    .map((ch, i) => (i === 0 ? ch.toUpperCase() : ch))
    .join("");
  return capitalized;
};
