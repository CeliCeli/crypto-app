import React, { Suspense } from "react";
import CombineProviders from "helpers/combineProviders";
import { GlobalStyle } from "styles/global";
import { CryptoProvider } from "context/cryptoContext";
import { Layout } from "modules/Layout";
import { View } from "react-navi";
import { EthereumProvider } from "context/web3Context";
import { FavoritesProvider } from "context/favoritesContext";

const providers = [CryptoProvider, FavoritesProvider];

const App = () => {
  return (
    <CombineProviders components={providers}>
      <GlobalStyle />
      <Layout>
        <Suspense fallback={null}>
          <View />
        </Suspense>
      </Layout>
    </CombineProviders>
  );
};

export default App;
