import { useEffect, useState } from "react";
import { Device } from "styles/sizes";

export const useCurrentMedia = (): Device | null => {
  const [currentMediaType, setCurrentMediaType] = useState<Device | null>(null);

  const handleResize = () => {
    const width = window.innerWidth;
    let mediaType: Device | null = Device.DESKTOP_XL;

    if (width <= Device.MOBILE_XS) {
      mediaType = Device.MOBILE_XS;
    } else if (width <= Device.MOBILE_S) {
      mediaType = Device.MOBILE_S;
    } else if (width <= Device.MOBILE) {
      mediaType = Device.MOBILE;
    } else if (width <= Device.TABLET) {
      mediaType = Device.TABLET;
    } else if (width <= Device.DESKTOP) {
      mediaType = Device.DESKTOP;
    }

    setCurrentMediaType(mediaType);
  };

  useEffect(() => {
    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return currentMediaType;
};
