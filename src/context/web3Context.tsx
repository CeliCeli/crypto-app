import { connectToEthereum, getBalance } from "api/action/web3";
import React, {
  FC,
  ReactNode,
  createContext,
  useEffect,
  useState,
} from "react";
import Web3 from "web3";

interface EthereumContextType {
  web3: Web3 | null;
  balance: string | null;
  setWalletAddress: (address: string) => void;
}

export const EthereumContext = createContext<EthereumContextType>({
  web3: null,
  balance: null,
  setWalletAddress: () => {},
});

export const EthereumProvider: FC<{ children?: ReactNode }> = ({
  children,
}) => {
  const [web3, setWeb3] = useState<Web3 | null>(null);
  const [balance, setBalance] = useState<string | null>(null);
  const [walletAddress, setWalletAddress] = useState<string>("");

  useEffect(() => {
    const connectToEthereumAndFetchBalance = async () => {
      const connectedWeb3 = await connectToEthereum();
      setWeb3(connectedWeb3);
      if (connectedWeb3 && walletAddress) {
        const walletBalance = await getBalance(walletAddress);
        setBalance(walletBalance);
      }
    };

    connectToEthereumAndFetchBalance();
  }, [walletAddress]);

  const handleSetWalletAddress = (address: string) => {
    setWalletAddress(address);
  };

  return (
    <EthereumContext.Provider
      value={{ web3, balance, setWalletAddress: handleSetWalletAddress }}
    >
      {children}
    </EthereumContext.Provider>
  );
};
