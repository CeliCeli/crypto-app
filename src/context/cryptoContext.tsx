import React, {
  createContext,
  FC,
  ReactNode,
  useEffect,
  useMemo,
  useState,
} from "react";
import { fetchCryptoData } from "api/action/api";
import { CryptoData } from "models/cryptoData";

interface CryptoContextProps {
  cryptoData: CryptoData[];
  cryptoItem: CryptoData | null;
  getCryptoById: (id: string, timing: string) => void;
  setDataInterval: (interval: "h1" | "h12" | "m1" | "m15") => void;
  isLoading: boolean;
}

export const CryptoContext = createContext<CryptoContextProps>({
  cryptoData: [],
  cryptoItem: null,
  setDataInterval: () => {},
  getCryptoById: () => {},
  isLoading: true,
});

interface CryptoProviderProps {
  children?: ReactNode;
}

export const CryptoProvider: FC<CryptoProviderProps> = ({ children }) => {
  const [cryptoData, setCryptoData] = useState<CryptoData[]>([]);
  const [cryptoItem, setCryptoItem] = useState<CryptoData | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [cryptoInterval, setCryptoInterval] = useState("m15");

  const fetchData = async () => {
    try {
      const data = await fetchCryptoData(undefined, cryptoInterval);
      setCryptoData(data);
      setIsLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
    const interval = setInterval(fetchData, 1000 * 60 * 3);

    return () => {
      clearInterval(interval);
    };
  }, [cryptoInterval]);

  const setDataInterval = (i: "h1" | "h12" | "m1" | "m15") =>
    setCryptoInterval(i);

  const getCryptoById = async (id: string, timing: string) => {
    setIsLoading(true);
    try {
      const data = await fetchCryptoData(id, timing);
      setCryptoItem(data[0]);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  const value = useMemo(() => {
    return {
      cryptoData,
      cryptoItem,
      isLoading,
      getCryptoById,
      setDataInterval,
    };
  }, [cryptoData, cryptoItem, getCryptoById, isLoading, setDataInterval]);

  return (
    <CryptoContext.Provider value={value}>{children}</CryptoContext.Provider>
  );
};
