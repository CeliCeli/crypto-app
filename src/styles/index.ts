import styled from "styled-components/macro";
import { media, padding, device } from "./sizes";

export const ScContainer = styled.div`
  max-width: calc(1440px + ${padding * 2}px);
  margin-left: auto;
  margin-right: auto;
  padding: 0 ${padding * 2}px;

  @media (max-width: 1500px) {
    max-width: calc(${device.desktop} + ${padding * 2}px);
  }
  @media (${media.desktop}) {
    max-width: calc(${device.tablet} + ${padding * 2}px);
  }
  @media (${media.tablet}) {
    max-width: calc(${device.mobile} + ${padding * 2}px);
  }
  @media (${media.mobile}) {
    max-width: calc(${device.mobileS} + ${padding * 2}px);
  }
  @media (${media.mobileS}) {
    max-width: calc(${device.mobileXS} + ${padding * 2}px);
  }
  @media (${media.mobileXS}) {
    max-width: 100%;
    padding: 0 16px;
  }
`;
