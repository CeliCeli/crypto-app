import { createGlobalStyle } from "styled-components";
import { theme } from "theme";

export const GlobalStyle = createGlobalStyle`
    @font-face {
      font-family: 'Switzer';
      src: url('/fonts/Switzer-Regular.eot') format('truetype'),
          url('/fonts/Switzer-Regular.woff') format('woff'),
          url('/fonts/Switzer-Regular.woff2') format('woff2');
      font-weight: 400;
      font-style: normal;
    }
    @font-face {
      font-family: 'Switzer';
      src: url('/fonts/Switzer-Bold.eot') format('truetype'),
          url('/fonts/Switzer-Bold.woff') format('woff'),
          url('/fonts/Switzer-Bold.woff2') format('woff2');
      font-weight: 700;
      font-style: normal;
    }
    @font-face {
      font-family: 'Switzer';
      src: url('/fonts/Switzer-Black.eot') format('truetype'),
          url('/fonts/Switzer-Black.woff') format('woff'),
          url('/fonts/Switzer-Black.woff2') format('woff2');
      font-weight: 900;
      font-style: normal;
    }


  *{
    box-sizing: border-box;
    outline: none;
  }
  
  body{
    margin: 0;
    background-color: ${theme.primary};
    color:${theme.white};
    font-family: 'Switzer';
    font-weight:400;
  }

  .logo{
    text-decoration: none;
    color:${theme.white};
    outline: none;
    display: inline-block;
    font-weight: 700;
    font-size:24px;
  }
`;
