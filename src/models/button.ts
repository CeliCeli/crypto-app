import { ReactNode } from "react";

export enum ButtonColor {
  Primary = "primary",
  Secondary = "secondary",
}

export interface ScButtonProps {
  color?: ButtonColor;
}

export interface ButtonProps extends ScButtonProps {
  text?: string;
  className?: string;
  children?: ReactNode;
  onClick?: (e?: React.MouseEvent) => void;
  disabled?: boolean;
}
