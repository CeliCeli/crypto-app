export const symbols = [
  "bitcoin",
  "ethereum",
  "monero",
  "litecoin",
  "cardano",
  "stellar",
  "eos",
  "neo",
  "dash",
];
export const enum ChartStatus {
  Up = "up",
  Down = "down",
}

export type InitData = {
  id: string;
  symbol: string;
  price: number;
  priceUsd?: string;
};

export type CryptoData = InitData & {
  history: number[];
  percent: string;
  status: ChartStatus;
  dates: string[];
  icon?: string;
};

export type CryptoHistory = {
  id: string;
  date: Date;
  priceUsd: string;
  time: number;
};

export type InintCryptoHistory = {
  data: CryptoHistory[];
  timestamp: number;
};
