import { hexToRGB } from "helpers/hexToRgb";
import styled from "styled-components/macro";
import { ScContainer } from "styles";
import { theme } from "theme";

export const ScHeader = styled.header`
  background-color: ${theme.secondary};
  padding: 16px 0;
  ${ScContainer} {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
export const ScMain = styled.section``;

export const ScNav = styled.div``;
export const ScFooter = styled.footer`
  height: 100px;
  background-color: ${theme.secondary};
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${hexToRGB(theme.white, 0.3)};
  font-size: 12px;
`;
