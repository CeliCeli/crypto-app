import React, { FC, ReactNode } from "react";
import { ScFooter, ScHeader, ScMain } from "./styled";
import { Follower } from "components/Follower";
import { ScContainer } from "styles";
import { Button } from "components/Button";
import { ButtonColor } from "models/button";

export const Layout: FC<{ children?: ReactNode }> = ({ children }) => {
  return (
    <>
      <ScHeader>
        <ScContainer>
          <a className="logo" href="/">
            {`⚡️ testapp`}
          </a>
          <nav>
            <Button color={ButtonColor.Secondary} text="Get ETH balance" />
          </nav>
        </ScContainer>
      </ScHeader>
      <ScContainer>{children}</ScContainer>
      {/* <ScFooter>Copyright © 2023</ScFooter> */}
      {/* <Follower speed={0.005} sensitivity={1} /> */}
    </>
  );
};
