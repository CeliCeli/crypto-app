import React from "react";
import { route } from "navi";
import { Live } from "./components";

export default route(async (req) => {
  const id = req.params.id;

  return {
    title: "testapp - Live",
    view: <Live id={id} />,
  };
});
