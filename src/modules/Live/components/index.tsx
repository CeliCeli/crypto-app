import React, {
  FC,
  useContext,
  useEffect,
  useMemo,
  useState,
  useRef,
} from "react";
import { ScLive } from "./styled";
import { CryptoContext } from "context/cryptoContext";
import { Loader } from "components/Loader";
import { Currencies } from "modules/Currencies";
import { Chart } from "components/Chart";
import { WebSocketSubscription, subscribeToSymbol } from "api/action/api";
import { CryptoData } from "models/cryptoData";
import { Card } from "modules/Currencies/Card";

export const Live: FC<{ id: string }> = ({ id }) => {
  const { cryptoItem, cryptoData, getCryptoById, isLoading } =
    useContext(CryptoContext);
  const [chartState, setChartState] = useState<CryptoData | undefined>();
  const subscriptionRef = useRef<WebSocketSubscription | null>(null);

  useEffect(() => {
    getCryptoById(id, "m1");
  }, [id]);

  useEffect(() => {
    if (subscriptionRef.current) {
      subscriptionRef.current.unsubscribe();
    }
    if (!cryptoItem) return;

    const subscription = subscribeToSymbol(setChartState, cryptoItem);
    subscriptionRef.current = subscription;

    return () => {
      if (subscriptionRef.current) {
        subscriptionRef.current.unsubscribe();
        subscriptionRef.current = null;
      }
    };
  }, [id, subscriptionRef, cryptoItem]);

  return (
    <ScLive>
      {isLoading || !cryptoItem || !chartState ? (
        <Loader />
      ) : (
        <>
          <Card isMain isList {...chartState} />
          <MemoizedChart key={chartState?.id} isFull data={chartState} />
          <Currencies
            isList
            title={"Crypto"}
            data={cryptoData.filter((el) => el.id !== id)}
          />
        </>
      )}
    </ScLive>
  );
};

const MemoizedChart = React.memo(Chart);
