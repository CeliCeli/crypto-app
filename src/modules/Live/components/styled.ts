import { ScButton } from "components/Button/styled";
import { ScCard } from "modules/Currencies/styled";
import styled from "styled-components";

export const ScLive = styled.div`
  padding-top: 32px;
  & > ${ScCard} {
    margin-bottom: 32px;
    .chart {
      display: none;
    }
    .cover {
      ${ScButton} {
        &:first-child {
          display: none;
        }
      }
    }
  }
`;
