import { ScButton } from "components/Button/styled";
import { hexToRGB } from "helpers/hexToRgb";
import { ChartStatus } from "models/cryptoData";
import styled, { css } from "styled-components/macro";
import { media } from "styles/sizes";
import { theme } from "theme";

export const ScCurrencies = styled.div<{ isList?: boolean }>`
  .container {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 32px 16px;
    position: relative;
    margin-bottom: 32px;
    &.empty {
      grid-template-columns: 1fr;
    }
  }
  .plug {
    min-height: 320px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid ${hexToRGB(theme.white, 0.1)};
    background: ${hexToRGB(theme.white, 0.01)};
    border-radius: 16px;
    font-size: 16px;
    line-height: 150%;
    color: ${hexToRGB(theme.white, 0.2)};
  }

  @media (max-width: 1500px) {
    .container {
      grid-template-columns: 1fr 1fr 1fr;
      gap: 24px 12px;
      margin-bottom: 24px;
    }
  }

  @media (${media.desktop}) {
    .container {
      grid-template-columns: 1fr 1fr;
      gap: 8px;
      margin-bottom: 16px;
    }
  }

  @media (${media.mobile}) {
    .container {
      grid-template-columns: 1fr;
      gap: 16px;
      margin-bottom: 16px;
    }
    .plug {
      min-height: 120px;
    }
  }

  ${({ isList }) =>
    isList &&
    css`
      .container {
        grid-template-columns: 1fr 1fr;
        gap: 8px;

        @media (max-width: 1500px) {
          grid-template-columns: 1fr;
        }
      }
    `}
`;

export interface ScCardProps {
  status: ChartStatus;
  isList?: boolean;
}
export const ScCard = styled.div<ScCardProps>`
  border-radius: 16px;
  background-color: ${theme.secondary};
  padding: 4px;
  position: relative;
  cursor: pointer;
  transition: 0.2s;

  .chart {
    border: 1px solid ${hexToRGB(theme.white, 0.02)};
    border-radius: 14px;
    padding: 16px;
    background-color: ${hexToRGB(theme.white, 0.01)};
  }
  .headline {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 8px 12px;
    position: relative;
  }

  .currency {
    font-size: 14px;
    color: ${hexToRGB(theme.white, 0.1)};
    margin-right: 12px;
  }

  .symbol {
    font-size: 20px;
    font-weight: 700;
    display: flex;
    align-items: center;
    gap: 8px;
    img {
      width: 24px;
      height: 24px;
      vertical-align: middle;
      background-color: ${hexToRGB(theme.white, 1)};
      border-radius: 50%;
      padding: 1px;
    }
  }

  .price {
    display: flex;
    align-items: center;
    gap: 4px;
  }

  .cover {
    position: absolute;
    left: 0;
    right: 0;
    top: 44px;
    bottom: 0;
    background-color: ${hexToRGB(theme.secondary, 0.2)};
    backdrop-filter: blur(4px);
    border-radius: 16px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    gap: 4px;
    overflow: hidden;
    opacity: 0;
    pointer-events: none;
    transition: 0.2s;

    ${ScButton} {
      &:first-child {
        width: 160px;
      }
      &.heart {
        padding: 0;
        opacity: 0.8;
        border: none;
        display: flex;
        align-items: center;
        gap: 8px;
        margin-left: -16px;

        &:hover {
          opacity: 1;
        }
        svg {
          vertical-align: middle;
          path {
            fill: ${theme.white};
          }
        }
      }
    }
  }

  &:hover {
    .cover {
      pointer-events: initial;
      opacity: 1;
    }
  }

  ${({ status }) => {
    switch (status) {
      case ChartStatus.Down:
        return css`
          .percent {
            color: ${theme.red};
          }
        `;
      default:
        return css`
          .percent {
            color: ${theme.green};
          }
        `;
    }
  }}

  ${({ isList }) =>
    isList &&
    css`
      display: flex;
      padding: 16px 16px 16px 24px;
      border-radius: 8px;
      gap: 16px;
      cursor: default;
      .symbol {
        gap: 24px;
      }
      .headline {
        padding: 0;
        flex: 1 1 auto;
      }
      .chart {
        flex: 0 0 120px;
        max-width: 120px;
        padding: 0;
        border-radius: 0;
        border: none;
        background: transparent;
      }
      .price {
        gap: 0;
        .amount {
          width: 100px;
          text-align: right;
          padding-right: 24px;
        }
      }
      .cover {
        position: static;
        flex-direction: row;
        opacity: 1;
        background-color: transparent;
        backdrop-filter: none;
        gap: 16px;
        overflow: visible;
        border-radius: 0;
        pointer-events: initial;
        ${ScButton} {
          height: 32px;
          &:first-child {
            width: auto;
          }
          &.heart {
            margin: 0;
            font-size: 0;
          }
        }
      }
    `}

  @media (${media.mobile}) {
    display: flex;
    padding: 16px 16px 16px 24px;
    border-radius: 8px;
    gap: 16px;
    cursor: default;
    .symbol {
      gap: 24px;
    }
    .headline {
      padding: 0;
      flex: 1 1 auto;
    }
    .chart {
      flex: 0 0 120px;
      max-width: 120px;
      padding: 0;
      border-radius: 0;
      border: none;
      background: transparent;
    }
    .price {
      gap: 0;
      .amount {
        width: 100px;
        text-align: right;
        padding-right: 24px;
      }
    }
    .cover {
      position: static;
      flex-direction: row;
      opacity: 1;
      background-color: transparent;
      backdrop-filter: none;
      gap: 16px;
      overflow: visible;
      border-radius: 0;
      pointer-events: initial;
      ${ScButton} {
        height: 32px;
        &:first-child {
          width: auto;
        }
        &.heart {
          margin: 0;
          font-size: 0;
        }
      }
    }
  }
  @media (${media.mobileS}) {
    padding: 12px;
    .symbol {
      gap: 16px;
      font-size: 16px;
      img {
        position: absolute;
        left: 0;
        top: 50%;
        width: 32px;
        height: 32px;
        margin-top: -16px;
      }
    }
    .currency {
      display: none;
    }
    .headline {
      flex: 1 1 auto;
      padding-left: 40px;
    }
    .chart {
      display: none;
    }
    .cover {
      ${ScButton}:first-child {
        display: none;
      }
    }
  }
  @media (${media.mobileXS}) {
    margin-bottom: 0;
    padding: 4px 8px;
    .price {
      .amount {
        padding-right: 12px;
      }
    }
    .headline {
      padding-left: 32px;
    }
    .symbol {
      gap: 16px;
      font-size: 12px;
      img {
        position: absolute;
        left: 0;
        top: 50%;
        width: 20px;
        height: 20px;
        margin-top: -10px;
      }
    }
  }
`;
