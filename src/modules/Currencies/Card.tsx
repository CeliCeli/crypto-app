import React, { FC, useContext, useMemo } from "react";
import { ScCard, ScCardProps } from "./styled";
import { CryptoData } from "models/cryptoData";
import { Chart } from "components/Chart";
import { Button } from "components/Button";
import { ButtonColor } from "models/button";

import { ReactComponent as Heart } from "assets/heart.svg";
import { ReactComponent as HeartFill } from "assets/heartFull.svg";

import classNames from "classnames";
import { useNavigation } from "react-navi";
import { Routes } from "models/routes";
import { camelize } from "helpers/camelize";
import { FavoritesContext } from "context/favoritesContext";
import { useCurrentMedia } from "hooks/useCurrentMediaType";
import { Device } from "styles/sizes";

type CardProps = CryptoData &
  ScCardProps & {
    isMain?: boolean;
  };

export const Card: FC<CardProps> = ({
  status,
  percent,
  history,
  id,
  symbol,
  price,
  icon,
  dates,
  isList,
  isMain,
}) => {
  const { addToFavorites, removeFromFavorites, favorites } =
    useContext(FavoritesContext);

  const { navigate } = useNavigation();
  const currentMediaType = useCurrentMedia();

  const handleClick = () => {
    navigate(`${Routes.Live}/${id}`);
  };

  const inFavorite = useMemo(() => favorites.includes(id), [id, favorites]);

  const handleAddToFavorites = () => {
    if (inFavorite) {
      removeFromFavorites(id);
    } else {
      addToFavorites(id);
    }
  };

  const isMobile: boolean = useMemo(() => {
    switch (currentMediaType) {
      case Device.MOBILE_S:
      case Device.MOBILE_XS:
        return true;

      default:
        return false;
    }
  }, [currentMediaType]);

  const handleGoLive = () => isMobile && handleClick();

  return (
    <ScCard isList={isList} status={status}>
      <div onClick={handleGoLive} className="headline">
        <div className="symbol">
          <img src={icon} alt={id} />
          {isMain ? `${camelize(id)} ( ${symbol} )` : symbol}
        </div>
        <div className="price">
          <span className="currency">
            {symbol}
            {` / `}
            USDT
          </span>
          <span className="amount"> {price.toFixed(2)}$ </span>
          <span className="percent">{percent}%</span>
        </div>
      </div>
      <div className="chart">
        <Chart data={{ status, percent, history, id, symbol, price, dates }} />
      </div>
      <div className="cover">
        <Button onClick={handleClick} color={ButtonColor.Primary}>
          Go live
        </Button>
        <Button
          onClick={handleAddToFavorites}
          className={classNames({ heart: true, active: false })}
        >
          {inFavorite ? <HeartFill /> : <Heart />}
          to Favorite
        </Button>
      </div>
    </ScCard>
  );
};
