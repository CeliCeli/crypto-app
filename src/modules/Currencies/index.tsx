import React, { FC } from "react";
import { Card } from "./Card";
import { CryptoData } from "models/cryptoData";
import { ScCurrencies } from "./styled";
import classNames from "classnames";

interface CurrenciesProps {
  title: string;
  data: CryptoData[] | null;
  isList?: boolean;
}

export const Currencies: FC<CurrenciesProps> = ({ title, data, isList }) => {
  return (
    <ScCurrencies isList={isList}>
      <h2>{title}</h2>
      <div
        className={classNames({
          container: true,
          empty: data === null || data.length === 0,
        })}
      >
        {data ? (
          data.length !== 0 ? (
            data.map((item, index) => (
              <Card {...item} isList={isList} key={index} />
            ))
          ) : (
            <div className="plug">Section is empty</div>
          )
        ) : (
          <div className="plug">
            This is the demo version.
            <br />
            The list of sections may not be complete
          </div>
        )}
      </div>
    </ScCurrencies>
  );
};
