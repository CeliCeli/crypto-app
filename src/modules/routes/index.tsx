import React from "react";
import { lazy, mount, route } from "navi";
import { Lobby } from "modules/Lobby/components";
import { Routes } from "models/routes";

export const routes = mount({
  [Routes.Default]: route({
    title: "Crypto-app",
    view: <Lobby />,
  }),
  [`${[Routes.Live]}/:id`]: lazy(() => import("../Live/route")),
});
