import React, { FC, useContext, useEffect, useMemo } from "react";
import { ScLobby } from "./styled";
import { CryptoContext } from "context/cryptoContext";
import { Currencies } from "modules/Currencies";
import { Loader } from "components/Loader";
import { FavoritesContext } from "context/favoritesContext";

export const Lobby: FC = () => {
  const { cryptoData, isLoading } = useContext(CryptoContext);
  const { favorites } = useContext(FavoritesContext);

  const favoritesData = useMemo(() => {
    return cryptoData.filter(({ id }) => favorites.includes(id));
  }, [cryptoData, favorites]);

  return (
    <ScLobby>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <Currencies title={"Crypto"} data={cryptoData} />
          <Currencies title={"Fiat"} data={null} />
          <Currencies title={"Favorite"} data={favoritesData || []} />
        </>
      )}
    </ScLobby>
  );
};
