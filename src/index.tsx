import React from "react";
import { createRoot } from "react-dom/client";
import { Router } from "react-navi";
import HelmetProvider from "react-navi-helmet-async";
import reportWebVitals from "./reportWebVitals";

import { routes } from "modules/routes";

import App from "App";

const container = document.getElementById("root");
const root = createRoot(container as Element);

root.render(
  <HelmetProvider>
    <Router routes={routes}>
      <App />
    </Router>
  </HelmetProvider>
);

reportWebVitals();
