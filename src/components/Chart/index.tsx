import React, { useEffect, useMemo, useRef } from "react";
import { ScSvg } from "./styled";
import { ChartStatus, CryptoData } from "models/cryptoData";
import { theme } from "theme";
import { v4 as uuid } from "uuid";
import { createChartSvg } from "./helpers";
import { useCurrentMedia } from "../../hooks/useCurrentMediaType";
import { Device } from "styles/sizes";

interface ChartProps {
  data: CryptoData;
  isFull?: boolean;
}

export const Chart: React.FC<ChartProps> = ({ data, isFull }) => {
  const svgRef = useRef<SVGSVGElement | null>(null);
  const gradientIdRef = useRef<string>(uuid().replace(/-/g, ""));

  const currentMediaType = useCurrentMedia();

  const currentColor = useMemo(() => {
    switch (data.status) {
      case ChartStatus.Down:
        return theme.red;
      default:
        return theme.green;
    }
  }, [data.status]);

  const memoizedData = useMemo(() => data, [data]);

  const gridSize: number = useMemo(() => {
    switch (currentMediaType) {
      case Device.DESKTOP:
      case Device.TABLET:
      case Device.MOBILE:
        return 60;
      case Device.MOBILE_S:
        return 50;
      case Device.MOBILE_XS:
        return 30;
      default:
        return 70;
    }
  }, [currentMediaType]);

  useEffect(() => {
    const handleResize = () => {
      if (svgRef.current && data.history.length > 0) {
        const svgElement = svgRef.current;
        const svgWidth = svgElement.clientWidth;
        const svgHeight = svgElement.clientHeight;
        const history = data.history;

        svgElement.innerHTML = "";

        createChartSvg(
          svgElement,
          svgWidth,
          svgHeight,
          history,
          currentColor,
          gradientIdRef.current,
          gridSize,
          isFull
        );
      }
    };

    window.addEventListener("resize", handleResize);

    handleResize();

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [memoizedData, currentColor, memoizedData.history, svgRef, isFull]);

  return <ScSvg ref={svgRef} />;
};
