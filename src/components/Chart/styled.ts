import styled from "styled-components/macro";
import { media } from "styles/sizes";

export const ScSvg = styled.svg`
  width: 100%;
  height: auto;
  aspect-ratio: 3/1;
  cursor: default;
  path {
    transition: 0.2s;
  }
  text {
    z-index: 999;
    opacity: 0.2;
    transition: 0.2s;
  }
  &:hover {
    text {
      opacity: 0.5;
    }
  }

  @media (${media.mobile}) {
    aspect-ratio: 2/1;
  }
  @media (${media.mobileS}) {
    aspect-ratio: 1/1;
  }
`;
