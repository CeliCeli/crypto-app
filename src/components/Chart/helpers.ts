import { formatDate } from "helpers/formatDate";

export const createChartSvg = async (
  svgElement: SVGSVGElement,
  svgWidth: number,
  svgHeight: number,
  history: number[],
  currentColor: string,
  id: string,
  gridSize: number,
  isFull?: boolean
) => {
  svgElement.innerHTML = "";

  const minValue = Math.min(...history);
  const maxValue = Math.max(...history);

  const pathData = history.map((price, index) => {
    const x = (index / (history.length - 1)) * svgWidth;
    const y =
      svgHeight - ((price - minValue) / (maxValue - minValue)) * svgHeight;
    return { x, y, value: price, date: Math.round(index / gridSize) };
  });
  const gradient = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "linearGradient"
  );
  gradient.setAttribute("id", `${id}`);
  gradient.setAttribute("gradientUnits", "userSpaceOnUse");
  gradient.setAttribute("x1", "0");
  gradient.setAttribute("y1", "0");
  gradient.setAttribute("x2", "0");
  gradient.setAttribute("y2", svgHeight.toString());

  const stop1 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
  stop1.setAttribute("offset", "0");
  stop1.setAttribute("stop-color", currentColor);
  stop1.setAttribute("stop-opacity", isFull ? "0.5" : "0.2");

  const stop2 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
  stop2.setAttribute("offset", "100%");
  stop2.setAttribute("stop-color", "transparent");

  gradient.appendChild(stop1);
  gradient.appendChild(stop2);

  const path1 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  path1.setAttribute(
    "d",
    `M ${pathData
      .map((p) => `${p.x},${p.y}`)
      .join(" L ")} L ${svgWidth},${svgHeight} L 0,${svgHeight} Z`
  );
  path1.setAttribute("fill", `url(#${id})`);
  path1.setAttribute("stroke", "none");

  const path2 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  const pathData2 = pathData.map((p) => `${p.x},${p.y}`);
  path2.setAttribute("d", `M ${pathData2.join(" L ")}`);
  path2.setAttribute("fill", "none");
  path2.setAttribute("stroke", currentColor);
  path2.setAttribute("stroke-width", "2");
  svgElement.appendChild(gradient);
  svgElement.appendChild(path1);
  svgElement.appendChild(path2);
  if (isFull) {
    for (let y = gridSize; y < svgHeight; y += gridSize) {
      const line = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "line"
      );
      line.setAttribute("x1", "0");
      line.setAttribute("y1", y.toString());
      line.setAttribute("x2", svgWidth.toString());
      line.setAttribute("y2", y.toString());
      line.setAttribute("stroke", "#ccc");
      line.setAttribute("stroke-width", "0.1");
      svgElement.appendChild(line);

      const valueText = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      valueText.setAttribute("x", "5");
      valueText.setAttribute("y", (y - 5).toString());
      valueText.setAttribute("fill", "#ccc");
      valueText.setAttribute("font-size", "14");
      valueText.textContent = `${(
        ((svgHeight - y) * (maxValue - minValue)) / svgHeight +
        minValue
      ).toFixed(2)}`;
      svgElement.appendChild(valueText);
    }

    for (let x = gridSize; x < svgWidth; x += gridSize) {
      const line = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "line"
      );
      line.setAttribute("x1", x.toString());
      line.setAttribute("y1", "0");
      line.setAttribute("x2", x.toString());
      line.setAttribute("y2", svgHeight.toString());
      line.setAttribute("stroke", "#ccc");
      line.setAttribute("stroke-width", "0.1");
      svgElement.appendChild(line);

      const date =
        pathData[Math.round((x / svgWidth) * (pathData.length - 1))].date;
      const dateText = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      dateText.setAttribute("x", x.toString());
      dateText.setAttribute("y", (svgHeight - 15).toString());
      dateText.setAttribute("fill", "#ccc");
      dateText.setAttribute("font-size", "12");
      dateText.setAttribute("text-anchor", "middle");
      dateText.setAttribute("alignment-baseline", "middle");
      dateText.textContent = date.toString();
      svgElement.appendChild(dateText);
    }
  }
};
