import React, { FC } from "react";
import { ScButton } from "./styled";
import { ButtonColor, ButtonProps } from "models/button";

export const Button: FC<ButtonProps> = ({
  disabled,
  text,
  children,
  color,
  className,
  onClick,
}) => {
  return (
    <ScButton
      className={className}
      color={color}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
      {text}
    </ScButton>
  );
};
