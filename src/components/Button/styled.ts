import styled, { css } from "styled-components/macro";
import { ButtonColor, ScButtonProps } from "models/button";
import { theme } from "theme";
import { hexToRGB } from "helpers/hexToRgb";

export const ScButton = styled.button<ScButtonProps>`
  background-color: transparent;
  border-radius: 8px;
  height: 48px;
  padding: 0 24px;
  border: 2px solid ${theme.white};
  color: ${theme.white};
  font-size: 14px;
  font-weight: 700;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }

  ${({ color }) => {
    switch (color) {
      case ButtonColor.Primary:
        return css`
          border: none;
          background: ${theme.green};
        `;
      case ButtonColor.Secondary:
        return css`
          border: 1px solid ${hexToRGB(theme.white, 0.3)};
          color: ${theme.green};
          opacity: 1;

          &:hover {
            opacity: 1;
            border: 1px solid ${hexToRGB(theme.green, 1)};
            background-color: ${theme.green};
            color: ${theme.white};
          }
        `;

      default:
        break;
    }
  }};
`;
