import React from "react";
import styled, { keyframes } from "styled-components";

const spinAnimation = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const LoaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  position: absolute;
  translate: -50%;
  left: 50%;
`;

const LoaderSpinner = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 4px solid #ccc;
  border-top-color: #555;
  animation: ${spinAnimation} 0.8s linear infinite;
`;

export const Loader = () => (
  <LoaderWrapper>
    <LoaderSpinner />
  </LoaderWrapper>
);
