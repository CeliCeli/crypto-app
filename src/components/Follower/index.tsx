import React, { useState, useEffect, MouseEvent, useRef } from "react";
import { ScFollower, ScFollowerContainer } from "./styled";

import ellipse from "assets/ellipse.png";

interface FollowerProps {
  speed: number;
  sensitivity: number;
}

interface Position {
  x: number;
  y: number;
}

export const Follower: React.FC<FollowerProps> = ({ speed, sensitivity }) => {
  const [position, setPosition] = useState<Position>({ x: 0, y: 0 });
  const [targetPosition, setTargetPosition] = useState<Position>({
    x: 0,
    y: 0,
  });
  const containerRef = useRef<HTMLDivElement>(null);
  const followerRef = useRef<HTMLDivElement>(null);
  const animationFrameRef = useRef<number | null>(null);

  useEffect(() => {
    const handleMouseMove = (event: MouseEvent) => {
      const targetX =
        event.clientX - (followerRef.current?.offsetWidth || 0) / 2;
      const targetY =
        event.clientY - (followerRef.current?.offsetHeight || 0) / 2;

      setTargetPosition({ x: targetX, y: targetY });
    };

    document.addEventListener(
      "mousemove",
      handleMouseMove as unknown as EventListener
    );

    return () => {
      document.removeEventListener(
        "mousemove",
        handleMouseMove as unknown as EventListener
      );
    };
  }, []);

  useEffect(() => {
    const containerWidth = containerRef.current?.offsetWidth || 0;
    const containerHeight = containerRef.current?.offsetHeight || 0;
    const maxX = window.innerWidth - containerWidth;
    const maxY = window.innerHeight - containerHeight;

    const updatePosition = () => {
      setPosition((prevPosition) => {
        const distanceX = targetPosition.x - prevPosition.x;
        const distanceY = targetPosition.y - prevPosition.y;

        if (Math.abs(distanceX) < 1 && Math.abs(distanceY) < 1) {
          return prevPosition;
        }

        const deltaX = distanceX * speed * sensitivity;
        const deltaY = distanceY * speed * sensitivity;

        const updatedX = Math.max(0, Math.min(prevPosition.x + deltaX, maxX));
        const updatedY = Math.max(0, Math.min(prevPosition.y + deltaY, maxY));

        return { x: updatedX, y: updatedY };
      });
    };

    const animate = () => {
      updatePosition();
      animationFrameRef.current = requestAnimationFrame(animate);
    };

    animate();

    return () => {
      if (animationFrameRef.current) {
        cancelAnimationFrame(animationFrameRef.current);
      }
    };
  }, [speed, sensitivity, targetPosition]);

  return (
    <ScFollowerContainer ref={containerRef}>
      <ScFollower
        ref={followerRef}
        style={{ transform: `translate(${position.x}px, ${position.y}px)` }}
      >
        <img src={ellipse} alt="" />
      </ScFollower>
    </ScFollowerContainer>
  );
};
