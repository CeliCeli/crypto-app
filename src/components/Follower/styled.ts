import styled from "styled-components";

export const ScFollowerContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  pointer-events: none;
`;
export const ScFollower = styled.div`
  width: 300px;
  height: 300px;
  opacity: 0.1;
  svg,
  img {
    width: 100%;
    height: 100%;
  }
`;
