import React, { useEffect, useState } from "react";

export const CoinCapWebSocket: React.FC = () => {
  const [prices, setPrices] = useState<{ [key: string]: number }>({});

  useEffect(() => {
    const socket = new WebSocket("wss://ws.coincap.io/prices?assets=bitcoin");

    socket.onopen = () => {
      console.log("WebSocket connection established.");
    };

    socket.onmessage = (event) => {
      const data = JSON.parse(event.data);
      setPrices(data);
    };

    socket.onclose = () => {
      console.log("WebSocket connection closed.");
    };

    return () => {
      socket.close();
    };
  }, []);

  return (
    <div>
      <h2>Crypto Prices</h2>
      <ul>
        {Object.entries(prices).map(([symbol, price]) => (
          <li key={symbol}>
            {symbol}: ${price}
          </li>
        ))}
      </ul>
    </div>
  );
};
