import axios from "axios";
import { CryptoData } from "models/cryptoData";

const API_URL = "https://min-api.cryptocompare.com/data/v2";

const fetchCryptoData = async (
  selectedCrypto: string,
  range: { from: number; to: number },
  lendth = 80
): Promise<CryptoData[]> => {
  const currentLength = lendth > 200 ? 10 : lendth;
  try {
    const response = await axios.get(
      `${API_URL}/histohour?fsym=${selectedCrypto}&tsym=USD&limit=${currentLength}&toTs=${range.to}`
    );
    const data = response.data;

    const updatedCryptoData: CryptoData[] = data.Data.Data.map((item: any) => ({
      time: item.time.toString(),
      price: item.close,
    }));

    return updatedCryptoData;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export { fetchCryptoData };
