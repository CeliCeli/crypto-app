import Web3 from "web3";

let web3: Web3 | null = null;

export const connectToEthereum = async (): Promise<Web3 | null> => {
  if (!(window as any).ethereum) {
    console.error("Web3 provider not found.");
    return null;
  }

  try {
    const ethereum = (window as any).ethereum;
    await ethereum.send("eth_requestAccounts");
    web3 = new Web3(ethereum);
    return web3;
  } catch (error) {
    console.error("Error connecting to Ethereum:", error);
    return null;
  }
};

export const getBalance = async (
  walletAddress: string
): Promise<string | null> => {
  if (!web3) {
    console.error("Web3 is not connected.");
    return null;
  }

  try {
    const balance = await web3.eth.getBalance(walletAddress);
    return balance.toString();
  } catch (error) {
    console.error("Error getting balance:", error);
    return null;
  }
};
