import axios from "axios";
import service from "../services/api.services";
import { CryptoData, symbols } from "models/cryptoData";
import { Dispatch, SetStateAction } from "react";

const currentTimestamp = Math.floor(Date.now());
const oneDayAgoTimestamp = Math.floor(Date.now() - 24 * 60 * 60 * 1000);

export type LiveChart = {
  x: number;
  y: number;
};
export interface WebSocketSubscription {
  unsubscribe: () => void;
}

export const fetchInitData = async (symbol?: string): Promise<CryptoData[]> => {
  if (!symbol) {
    const responses = await Promise.all(
      symbols.map((symbol) =>
        axios.get(`https://${process.env.REACT_APP_API_URL}/${symbol}`)
      )
    );
    return service.createInitData(
      responses.map((response) => response.data.data)
    );
  } else {
    const responce = await axios.get(
      `https://${process.env.REACT_APP_API_URL}/${symbol}`
    );

    return service.createInitData([responce.data.data]);
  }
};

export const fetchIcons = async (data: CryptoData[]) => {
  return await axios
    .get(`${process.env.REACT_APP_ICONS_API_URL}`, {
      headers: { "X-CoinAPI-Key": process.env.REACT_APP_ICONS_API_KEY },
    })
    .then((res) => {
      return data.map((el) => ({
        ...el,
        icon:
          res.data.filter(
            (e: { asset_id: string; url: string }) => e.asset_id === el.symbol
          )[0]?.url || "",
        symbol: el.symbol,
      }));
    });
};

export const fetchCryptoHistory = async (
  data: CryptoData[],
  timing?: string
): Promise<CryptoData[]> => {
  const dataWithHistory = (await Promise.all(
    data.map(({ id, ...el }) =>
      axios
        .get(
          `https://${process.env.REACT_APP_API_URL}/${id}/history?interval=${
            timing ?? `m15`
          }&start=${oneDayAgoTimestamp}&end=${currentTimestamp}`,
          {
            headers: {
              Authorization: `Bearer ${process.env.AUTH_TOKEN}`,
            },
          }
        )
        .then((res) => {
          const history = service.addHistoryData(res.data);
          const info = service.getPercent(
            String(history[0]),
            String(history[history.length - 1])
          );
          const dates = service.addDate(res.data);
          return {
            ...el,
            percent: info.percent,
            status: info.status,
            id,
            history,
            dates,
          };
        })
    )
  )) as CryptoData[];

  return dataWithHistory;
};

export const fetchCryptoData = async (
  symbol?: string,
  timing?: string
): Promise<CryptoData[]> =>
  await fetchInitData(symbol)
    .then((res) => fetchCryptoHistory(res, timing))
    .then((res) => fetchIcons(res));

export const subscribeToSymbol = (
  setData: Dispatch<SetStateAction<CryptoData | undefined>>,
  initData?: CryptoData
) => {
  let pricesWs: WebSocket | null = null;

  const handleMessage = (msg: MessageEvent) => {
    try {
      const data = JSON.parse(msg.data);
      if (!data || !initData) return;
      const parsedData = data[initData.id];
      const newPrice = Number(parsedData);
      const newHistory = [...initData.history.slice(1), newPrice];
      const info = service.getPercent(
        String(newHistory[0].toFixed(2)),
        String(newHistory[newHistory.length - 1].toFixed(2))
      );
      const newData: CryptoData = {
        ...initData,
        history: newHistory,
        percent: info.percent,
        status: info.status,
        price: newPrice,
      };
      setData(newData);
    } catch (error) {
      console.error("Error parsing JSON:", error);
    }
  };

  const connect = () => {
    pricesWs = new WebSocket(
      `wss://ws.coincap.io/prices?assets=${initData?.id}`
    );
    pricesWs.onmessage = handleMessage;
  };

  const disconnect = () => {
    if (pricesWs) {
      pricesWs.onmessage = null;
      pricesWs.close();
      pricesWs = null;
    }
  };

  const handleSymbolChange = (newSymbol: string) => {
    if (!initData) return;
    if (newSymbol !== initData?.id) {
      disconnect();
      initData.id = newSymbol;
      connect();
    }
  };

  if (initData) {
    setData(initData);
    connect();
  }

  return {
    unsubscribe: disconnect,
    changeSymbol: handleSymbolChange,
  };
};
