import Decimal from "decimal.js";

import {
  ChartStatus,
  CryptoData,
  InintCryptoHistory,
  InitData,
} from "models/cryptoData";

const createInitData = (data: InitData[]): CryptoData[] => {
  return data.map((item) => ({
    id: item.id,
    symbol: item.symbol,
    price: parseFloat(item.priceUsd || "0"),
    history: [],
    percent: "",
    dates: [],
    status: ChartStatus.Up,
  }));
};

const addHistoryData = (data: InintCryptoHistory): number[] => {
  return data.data.map(({ priceUsd }) => Number(priceUsd));
};

const addDate = (data: InintCryptoHistory): string[] => {
  return data.data.map(({ date }) => String(date));
};

const getPercent = (
  previousPrice: string,
  currentPrice: string
): { status: any; percent: string } => {
  const previous = new Decimal(previousPrice);
  const current = new Decimal(currentPrice);

  const priceChange = current.minus(previous);
  const priceChangePercentage = priceChange.div(previous).times(100);

  const sign = priceChangePercentage.greaterThan(0) ? "+" : "-";

  return {
    status: sign === "+" ? ChartStatus.Up : ChartStatus.Down,
    percent: `${sign}${priceChangePercentage.abs().toFixed(2)}`,
  };
};

export default { createInitData, addHistoryData, getPercent, addDate };
