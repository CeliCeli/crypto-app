# Project Title

The project title - crypto test app.

## Design Decisions

In this project, I have chosen a simple and user-friendly design. The goal was to create a clear and intuitive interface that effectively demonstrates the fulfillment of the technical requirements. The design focuses on clarity and ease of use, providing a straightforward understanding and interaction for users with the project.

Instead of using Redux, I opted to use React Context for state management. Considering the size and requirements of the project, React Context provides a simpler and lightweight solution. It eliminates the need for additional libraries and boilerplate code associated with Redux, making the project easier to maintain and support.

## How to Run the Project

To run the project locally, follow the instructions below:

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Install the necessary dependencies by running: `yarn install`.
4. Start the development server by running: `yarn start`.
5. Open your browser and go to `http://localhost:3000` to access the project.

## Usage

The project is designed to be straightforward to use and understand. Simply follow the instructions or prompts provided within the application to navigate and interact with its features. If you encounter any issues or have questions, refer to the documentation or reach out to the project maintainers for assistance.


---

Note: Please let me know if you need any further assistance or if you have any more questions!
